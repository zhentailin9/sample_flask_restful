FROM python:3.7

ENV APP /app
ENV PYTHONUNBUFFERED 1 # python 執行訊息重新顯示到標準輸出


RUN mkdir $APP
WORKDIR $APP

EXPOSE 80 5000

COPY ./requirements.txt .

# Install Python dependencies
RUN /usr/local/bin/python -m pip install --upgrade pip && \
    pip3 install -r requirements.txt

CMD [ "python3", "app.py" ]

